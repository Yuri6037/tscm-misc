--@name GlassToggeler
--@author Yuri6037
--@class processor
--@autoupdate

--[[
Copyright (c) 2021 Yuri6037

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
]]--

wire.createInputs({"Prop", "Activate"}, {"Entity", "Normal"})
wire.createOutputs({"Active"}, {"Normal"})

local Prop = nil
local OnMat = "sprops/textures/sprops_rubber"
local SubMats = {}

function string.StartsWith(String, Start)
	return string.sub(String, 1, string.len(Start)) == Start
end

local function FindMaterials()
	if (string.StartsWith(Prop:model(), "models/smallbridge")) then
		for k, v in pairs(Prop:getMaterials()) do
			if (v == "spacebuild/bodyskin") then
				table.insert(SubMats, {k - 1, "spacebuild/glass", Prop:getSubMaterial(k - 1)})
			end
		end
	else
		for k, v in pairs(Prop:getMaterials()) do
			if (v == "cmats/glass" or v == "spacebuild/glass") then
				table.insert(SubMats, {k - 1, v, OnMat})
			end
		end
	end
end

Prop = wire.ports["Prop"]
if (IsValid(Prop)) then
	FindMaterials()
end

hook("input", "GlassToggeler", function(inp, val)
	if (inp == "Prop") then
		Prop = val
		SubMats = {}
		if (IsValid(Prop)) then
			FindMaterials()
		end
	elseif (inp == "Activate" and IsValid(Prop)) then
		if (val == 1) then
			for k, v in pairs(SubMats) do
				Prop:setSubMaterial(v[1], v[3])
			end
			wire.ports["Active"] = 1
		else
			for k, v in pairs(SubMats) do
				Prop:setSubMaterial(v[1], v[2])
			end
			wire.ports["Active"] = 0
		end
	end
end)
