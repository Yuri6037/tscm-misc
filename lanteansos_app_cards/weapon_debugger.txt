--@name Lanteans OS AppCard (WeaponDebugger)
--@author Yuri6037
--@class processor
--@model models/props_junk/sawblade001a.mdl

--[[
Copyright (c) 2021 Yuri6037

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
]]--

--Initialisation
Entity = ents.self()
Entity:setMaterial("phoenix_storms/black_chrome")
wire.createOutputs({"AppName", "AppCode"}, {"String", "String"})
Ports = wire.ports
--End

--Application
AppName = "weapondebug"
AppCode = [[
local app = {}
function app:Init()
	self:EnableAINetContext()
	self:SetTitle("Weapon Debugger")
	self:SetSize(256, 166)
	self:SetPos(LOCATION_CENTER)

	self.P = self:AddComponent("progress", 10, 32, COLOR(255, 255, 0), "Progress")
	self.P:SetSize(AUTO, 32)
	self.S = self:AddComponent("progress", 10, 64, COLOR(255, 0, 0), "Shield lost")
	self.S:SetSize(AUTO, 32)
	self.H = self:AddComponent("progress", 10, 96, COLOR(0, 0, 255), "Hull lost")
	self.H:SetSize(AUTO, 32)
	self.A = self:AddComponent("progress", 10, 128, COLOR(0, 255, 255), "Armor lost")
	self.A:SetSize(AUTO, 32)

	self:AddPullDownMenu("    Extension    ")
	self:AddPullDownMenuItem("StartSampling", function()
		OS.Dialog("code", "Please enter the amount of samples to read :", function(app)
			local code = tonumber(app:GetCodeStr())
			AIRunFunc("DBGStartSampling", code)
			app:Exit()
		end)
	end)
	self:AddPullDownMenuItem("StopSampling", function()
		AIRunFunc("DBGStopSampling")
	end)
end
function app:OnDataReceived(data)
	self.P:SetValue(math.round(data["CurSample"] / data["NumSamples"], 3))
	self.S:SetValue(math.round(data["PShieldLost"], 3))
	self.H:SetValue(math.round(data["PHullLost"], 3))
	self.A:SetValue(math.round(data["PArmorLost"], 3))
end
OS.DefineApp("weapondebug", app)
OS.AddAppMenuItem("weapondebug", "Weapon Debugger", "LanAI")
]]
--End

--Saving
Ports["AppName"] = "app_" .. AppName
Ports["AppCode"] = AppCode
--End
