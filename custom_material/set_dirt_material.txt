--@name SetDirtMaterial
--@author Yuri6037
--@class processor

--[[
Copyright (c) 2021 Yuri6037

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
]]--

ForceMat = "Iziraider/artifacts/quantum_mirror"

function GetGroundProp()
    local tr = trace.trace(ents.self():getPos(), ents.self():getPos() + ents.self():up() * -200, {ents.self()}, trace.MASK_SOLID)
    print(tr.Entity)
    if (IsValid(tr.Entity)) then
        return tr.Entity
    end
    return nil
end

if (not(GetGroundProp() == nil)) then
    local e = GetGroundProp()
    e:setMaterial(ForceMat)
    e:setColor(173, 145, 120)
end
ents.self():remove()